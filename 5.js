// 5. Sort following array low to high number without any library and builth in array methods
const array =  [1,3,4,3,2,3,0,10,30,30,7];

const bubbleSort =(arr)=>{
    let len = arr.length;
    for (let i = len-1; i>=0; i--){
      for(var j = 1; j<=i; j++){
         if(arr[j-1]>arr[j]){
            let temp = arr[j-1];
            arr[j-1] = arr[j];
            arr[j] = temp;
         }
      }
    }
    return arr;
};
console.log(bubbleSort(array));