const { expect } = require('chai');
const  {fibo}  = require('./fibo');

describe('test the fibonancy function',()=>{
    it('when number is 7 output must 13', (done)=>{
        const result = fibo(7);
        expect(result).to.equal(13);
        done();
    });
});