// 7. Use unit testing in your program. Choose one of the numbers 5, 6, or 7 from the above cases. 

const fibo = (n)=>{
    if(n<=1)return n; 
    return fibo(n - 2) + fibo(n-1); 
};
module.exports = {fibo}
